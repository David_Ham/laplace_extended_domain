from firedrake import *
import numpy as np

free_surface_id = 6 # in the used mesh
# domain dimensions
Lx = 2.
Lz = 1.
mesh = Mesh("L_domain.msh")

V = FunctionSpace( mesh, "CG", 1 )
V_DG0 = FunctionSpace( mesh, "DG", 0 )

def Heaviside_function( zero_on_left=True, jump_position=0. ):
    sign = 1.
    if zero_on_left == False:
        sign = -1.
    Hx = Function( V_DG0 )
    Hx_expr = Expression( "0.5*(1.0 + copysign( 1.0, sign*(x[0]-Lp) ))",
                            sign=sign, Lp=jump_position)
    Hx.interpolate( Hx_expr )
    
    # map to CG1 space
    I_cg = Function( V )
    
    par_loop('for (int i=0; i<A.dofs; i++) A[i][0] = fmax(A[i][0], B[0][0]);', dx,
        {'A' : (I_cg, RW), 'B': (Hx, READ)})
    return Hx, I_cg

def beyond_fluid_BCs():
    class MyBC(DirichletBC):
        def __init__(self, V, value, markers):
            # Call superclass init
            # We provide a dummy subdomain id.
            super(MyBC, self).__init__( V, value, 0 )
            # Override the "nodes" property which says where the boundary
            # condition is to be applied.
            self.nodes = np.unique(np.where(markers.dat.data_ro_with_halos == 0)[0])
    
    # Now I can use MyBC to create a "boundary condition" to zero out all
    # the nodes that are *not* in the fluid domain:
    _, I_f = Heaviside_function( zero_on_left=False )
    return MyBC( V, 0, I_f )

def in_fluid_BCs(): # needed to map analytical solution onto the fluid domain
    class MyBC(DirichletBC):
        def __init__(self, V, value, markers):
            # Call superclass init
            # We provide a dummy subdomain id.
            super(MyBC, self).__init__(V, value, 0)
            # Override the "nodes" property which says where the boundary
            # condition is to be applied.
            self.nodes = np.unique(np.where(markers.dat.data_ro_with_halos == 1)[0]) # here 1, not 0
    
    # Now I can use MyBC to create a "boundary condition" to select all
    # the nodes that are in the fluid domain:
    _, I_f = Heaviside_function( zero_on_left=False )
    return MyBC( V, 0, I_f )

BC_beyond_fluid = beyond_fluid_BCs()
BC_in_fluid = in_fluid_BCs()

phi = Function( V, name="phi" )
phi_s = Function( V )
phi_exact = Function( V, name="phi_exact" )
trial = TrialFunction( V )
v = TestFunction( V )

def analytical_solution( n=1, a=0.1, b=0):
    lambda_x = pi * n / Lx
    omega = sqrt( lambda_x * tanh( lambda_x * Lz) )
    phi.assign(0.)
    surface = DirichletBC( V, 0, free_surface_id )
    
    def compute_phi_exact(t):
        return Expression(
        "(a*cos(omega*t) + b*sin(omega*t)) * cos(lambda_x*x[0]) * cosh(lambda_x*x[1])",
                      omega=omega, a=a, b=b, Lz=Lz, t=t, lambda_x=lambda_x )
                      
    phi_s.assign( project( compute_phi_exact(0.), V ), subset=surface.node_set )
    phi_exact.assign( project( compute_phi_exact(0.), V ), subset=BC_in_fluid.node_set )


analytical_solution()

I, _ = Heaviside_function(zero_on_left=False)

BC_surface = DirichletBC( V, phi_s, free_surface_id )

a = dot( grad(trial), grad(v) ) * I * dx
L = Constant(0.) * v * I * dx

solve( a==L, phi, bcs=[BC_surface, BC_beyond_fluid] )

File("results/phi.pvd").write(phi)
File("results/phi_exact.pvd").write(phi_exact)

phi_diff = Function(V, name="phi_diff")
phi_diff.assign( sqrt( (phi - phi_exact)**2 ) )
File("results/phi_diff.pvd").write(phi_diff)
L2_error = sqrt( assemble( ( I*phi - I*phi_exact)**2 * dx ) )
print 'L2 error = ', L2_error


